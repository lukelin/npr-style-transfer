#!/usr/bin/env python
# coding: utf-8

# In[10]:


import numpy as np
import cv2


# In[11]:


def readfile():
    #s_img = cv2.imread('04_source.png')
    #r_img = cv2.imread('04_reference.png')
    src_name = input("Please input the source image name(ex : 01_source.png) : ")
    ref_name = input("Please input the reference image name(ex : 01_reference.png) : ")
    s_img = cv2.imread(src_name)
    r_img = cv2.imread(ref_name)
    
    return s_img, r_img

def image_state(image):
    (l, a, b) = cv2.split(image)
    (l_mean,l_std) = (l.mean(), l.std())
    (a_mean,a_std) = (a.mean(), a.std())
    (b_mean,b_std) = (b.mean(), b.std())
    
    return (l_mean, l_std, a_mean, a_std, b_mean, b_std)
    
    
def color_transfer(s_img ,r_img):
    h, w, c = s_img.shape
    r_img = cv2.resize(r_img,(h,w),interpolation=cv2.INTER_CUBIC)
    source = cv2.cvtColor(s_img, cv2.COLOR_BGR2LAB).astype("float32") #把RGB的圖片轉成LAB形式(與亮度有關)
    reference = cv2.cvtColor(r_img, cv2.COLOR_BGR2LAB).astype("float32")
    
    #cv2.imshow('Source Image', source)
    #cv2.imshow('Reference Image', reference)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    
    (l_mean_src, l_std_src, a_mean_src, a_std_src, b_mean_src, b_std_src) = image_state(source)
    (l_mean_ref, l_std_ref, a_mean_ref, a_std_ref, b_mean_ref, b_std_ref) = image_state(reference)
    
    
    (l,a,b) = cv2.split(source) #分開讀取LAB通道後減掉reference image的平均
    
    l -= l_mean_src
    a -= a_mean_src
    b -= b_mean_src
    
    l = (l_std_ref / l_std_src) * l 
    a = (a_std_ref / a_std_src) * a 
    b = (b_std_ref / b_std_src) * b
    
    l += l_mean_ref
    a += a_mean_ref
    b += b_mean_ref
    
    l = np.clip(l,0,255) #wrap the array value in [0-255]
    a = np.clip(a,0,255)
    b = np.clip(b,0,255)
    
    transfer = cv2.merge([l, a, b])
    transfer = cv2.cvtColor(transfer.astype("uint8"), cv2.COLOR_LAB2BGR)
    
    
    return transfer 
    


# In[12]:


(s_img, r_img) = readfile()
t_img = color_transfer(s_img, r_img)


cv2.imshow('Source Image', s_img)
cv2.imshow('Reference Image', r_img)
cv2.imshow('Transfer Image', t_img)
cv2.waitKey(0)
cv2.destroyAllWindows()


# In[ ]:




